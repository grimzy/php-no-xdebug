# PHP No XDebug

Bash script to run a PHP script on the command line without XDebug (when XDebug is enabled globally).

Use `phpnox` instead of `php` when running intensive PHP operations on an XDebug enabled environment.

## Installation

Clone the repository:

```shell
git clone git@bitbucket.org:grimzy/php-no-xdebug.git
```

Create a symlink in one of the directories in `$PATH`:

```
cd ~/bin
ln -s path/to/php-no-xdebug/phpnox
```

## Usage

Use `phpnox` instead of `php` when on the command line.

```shell
phpnox <command|file> [<command parameters>]
```

Examples:

```shell
phpnox composer install
phpnox index.php
```

## Credits

Credits go to [Joyce Babu](http://stackoverflow.com/users/465590/joyce-babu) for hist [answer on StackOverflow](http://stackoverflow.com/a/36723363/2657607).